In this project we will build a question / answer application.

## Introduction

This application is development only with ruby on rails and the main purpose is to 
posts question and receive answers by other users.

## Description

### Users

As user, I can:

* Post qusetion
* Answer a question.
* Manage my questions.

Below is represented the main page where the user is presented with the most recent questions.

![Main Page](https://bitbucket.org/negrei_petru/ask_me/raw/master/screenshots/main_page.png)

![Register Login](https://bitbucket.org/negrei_petru/ask_me/raw/master/screenshots/register_login.png)

![Registration](https://bitbucket.org/negrei_petru/ask_me/raw/master/screenshots/registration.png)

### Questions

Then the user login in the system and he can now post his own question and answer to the others.

![Question](https://bitbucket.org/negrei_petru/ask_me/raw/master/screenshots/question_post.png)

![Answer](https://bitbucket.org/negrei_petru/ask_me/raw/master/screenshots/answer.png)